1. How long did you spend on the coding test? What would you add to your solution if you had more time? If you didn't spend much time on the coding test then use this as an opportunity to explain what you would add.

I spent one hour and twelve minutes tracked with a stopwatch. If I had more time I would focus on the API request erros I kept having, since I was unable to make sucessful API data requests. Give more time I think this would be a problem I could fix.


Also given sufficient time I could maybe focus on the stylistic components of the app.

2. What was the most useful feature that was added to the latest version of your chosen language? Please include a snippet of code that shows how you've used it.


I have only a few hours of experience with Angular.js which means I am quite new to it, and it proved quite a challenge in the test overall.

However in the course I have been doing about it, I learned about programming with observables. 
With observables you can handle asynchronous operations, such as making HTTP requests and receiving data from APIs. I used it exaclty for this purpose in my code, the getRestaurants() method in RestaurantService returns an observable, which is then subscribed to in the RestaurantListComponent to receive the restaurant data.
Here's the code for it:

this.restaurantService.getRestaurants(this.outcode).subscribe(
  (data: any[]) => {
    this.restaurants = data;
  },
  (err: any) => {
    console.error(err);
  }
);



3. How would you track down a performance issue in production? Have you ever had to do this?

I never had to do this actually but here's what I would do:

1) I would attempt to reproduce the problem, meaning analyzing specific user scenarios that trigger the issue. By reproducing the problem, I would then focus on the area of the code involved in it.

2) Trace the flow of database and/or API requests, and checking for excessive queries

3) In the case of a database I would look at slow queries.

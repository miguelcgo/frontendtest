import { Component, Input, OnInit } from '@angular/core';
import { RestaurantService } from '../restaurant.service';

@Component({
  selector: 'app-restaurant-list',
  templateUrl: './restaurant-list.component.html',
  styleUrls: ['./restaurant-list.component.css']
})
export class RestaurantListComponent implements OnInit {
  @Input() outcode!: string;
  restaurants: any[] = [];

  constructor(private restaurantService: RestaurantService) { }

  ngOnInit(): void {
    this.getRestaurants();
  }

  getRestaurants() {
    this.restaurantService.getRestaurants(this.outcode).subscribe(
      (data: any[]) => {
        this.restaurants = data;
      },
      (err: any) => {
        console.error(err);
      }
    );
  }

  getRestaurantsByOutcode(outcode: string) {
    this.outcode = outcode;
    this.getRestaurants();
  }
}

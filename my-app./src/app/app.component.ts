import { Component, ViewChild } from '@angular/core';
import { RestaurantListComponent } from './restaurant-list/restaurant-list.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  outcode: string = 'EC4M'; // Default outcode

  @ViewChild(RestaurantListComponent) restaurantListComponent!: RestaurantListComponent;

  submitOutcode() {
    // You can perform any additional logic here before updating the restaurants
    this.restaurantListComponent.getRestaurantsByOutcode(this.outcode);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError,map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {

  constructor(private http: HttpClient) { }

  getRestaurants(outcode: string): Observable<any> {
    return this.http.get('https://uk.api.just-eat.io/restaurants/bypostcode/' + outcode).pipe(
      map((response: any) => response.Restaurants.filter((restaurant: any) => restaurant.IsOpenNow)),
      catchError(this.handleError)
    );
  }
  
  
  private handleError(error: Error | any) {
    let errorMessage = (error.message) ? error.message : 
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errorMessage);
    return throwError(errorMessage);
  }
  
 
}

const express = require('express');
const axios = require('axios');
const cors = require('cors');
const app = express();

app.use(cors());

app.get('/api/restaurants/:postcode', async (req, res) => {
    try {
        const response = await axios.get(`https://uk.api.just-eat.io/restaurants/bypostcode/${req.params.postcode}`);
        res.json(response.data);
    } catch (error) {
        res.status(500).json({ message: 'Error retrieving data' });
    }
});

app.listen(3000, () => console.log('Server running on port 3000'));